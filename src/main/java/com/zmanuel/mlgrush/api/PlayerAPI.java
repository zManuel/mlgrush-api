package com.zmanuel.mlgrush.api;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.ReplaceOptions;
import com.zmanuel.mlgrush.mongo.Database;
import lombok.Data;
import org.bson.Document;
import org.bukkit.entity.Player;

@Data
public class PlayerAPI {

    private Player player;
    private int wins;
    private int loses;
    private int elo;
    private int points;



    public PlayerAPI(Player player) {
        this.player = player;
        this.wins = 0;
        this.loses = 0;
        this.elo = 1000;
        this.points = 0;
        load();
    }

    private void load(){
        Document document = Database.getInstance().getPlayers().find(Filters.eq("uuid", player.getUniqueId().toString())).first();

        if (document == null) {
            this.saveData();
            return;
        }

        Document statisticsDocument = (Document) document.get("stats");
        this.wins = statisticsDocument.getInteger("wins");
        this.loses = statisticsDocument.getInteger("loses");
        this.elo = statisticsDocument.getInteger("elo");
        this.points = statisticsDocument.getInteger("points");
    }

    public void saveData(){
        Document document = new Document();
        Document statisticsDocument = new Document();
        statisticsDocument.put("wins", this.wins);
        statisticsDocument.put("loses", this.loses);
        statisticsDocument.put("elo", this.elo);
        statisticsDocument.put("points", this.points);
        document.put("uuid", this.getPlayer().getUniqueId().toString());
        document.put("stats", statisticsDocument);
        Database.getInstance().getPlayers().replaceOne(Filters.eq("uuid", this.getPlayer().getUniqueId().toString()), document, new ReplaceOptions().upsert(true));
    }


}

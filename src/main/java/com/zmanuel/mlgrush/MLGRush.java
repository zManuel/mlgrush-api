package com.zmanuel.mlgrush;

import com.zmanuel.mlgrush.api.PlayerAPI;
import com.zmanuel.mlgrush.configuration.YamlConfig;
import lombok.Getter;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class MLGRush extends JavaPlugin {

    @Getter
    private static MLGRush instance;

    @Getter private FileConfiguration config;

    @Override
    public void onEnable() {
        instance = this;
        YamlConfig.create("config", true);
        this.config = YamlConfig.getConfiguration("config");
    }

    public PlayerAPI getPlayer(Player player){
        return new PlayerAPI(player);
    }

}

package com.zmanuel.mlgrush.mongo;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.zmanuel.mlgrush.MLGRush;
import lombok.Getter;
import org.bson.Document;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.Collections;

@Getter
public class Database {

    @Getter
    private static Database instance;
    private MongoClient client;
    private MongoDatabase database;
    private MongoCollection<Document> players;
    private MongoCollection<Document> arenas;
    private MongoCollection<Document> configuration;

    public Database() {
        if(instance != null){
            throw new RuntimeException("The mongo database has already been instantiated.");
        }

        instance = this;

        FileConfiguration config = MLGRush.getInstance().getConfig();
        if(config.getBoolean("MONGO.AUTH.ENABLED")){
            final String USERNAME = config.getString("MONGO.AUTH.USERNAME");
            final String PASSWORD = config.getString("MONGO.AUTH.PASSWORD");
            final String DATABASE = config.getString("MONGO.AUTH.DATABASE");
            MongoCredential credential = MongoCredential.createCredential(USERNAME, DATABASE, PASSWORD.toCharArray());
            this.client =  new MongoClient(new ServerAddress(config.getString("MONGO.HOST"), config.getInt("MONGO.PORT")), Collections.singletonList(credential));
        }else{
            this.client = new MongoClient(new ServerAddress(config.getString("MONGO.HOST"), config.getInt("MONGO.PORT")));
        }
        this.database = this.client.getDatabase("MlgRush");
        this.players = this.database.getCollection("players");
        this.arenas = this.database.getCollection("arenas");
        this.configuration = this.database.getCollection("configuration");
    }

}